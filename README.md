# README #
![Scheme](/Images/Edge_Logo.jpg)

Edge Academy: Introduction to Continuous Integration and Continuous Delivery 

### What is this repository for? ###

This Repository is used to provided practical examples and excerises for 
Version: Alpha Release 0.1 - Archimedes 


### Who do I talk to? ###

Edge Academy is headed up by;
Duncan Small
Email Address: <duncan.small@edgetesting.co.uk>

For support please email:
Email Address: Edge Helpdesk <support@edgetesting.zendesk.com>

Technical Support:
Matthew Clarke
Email Address: <matthew.clarke@edgetesting.co.uk>