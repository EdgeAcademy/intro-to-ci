You can run unit tests by running the following command at the command prompt:

    mvn clean test

You can run automated tests by running the following command at the command prompt:

    mvn clean verify -P automated-test
    
